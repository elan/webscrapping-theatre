import os
from bs4 import BeautifulSoup, NavigableString


def delete():
    try:
        os.remove("public/index.html")
    except OSError:
        pass

def init():
    
    html = """
    <!DOCTYPE html>
    <html>
    <head>
        <link href="style.css" rel="stylesheet" type="text/css" />
        <meta charset="utf-8">
        <title>Webscrapping théâtre</title>
    </head>
    <body>
        <h1>Résultats webscrapping</h1>
        <table>
            <tr>
                <th>
                    Scène
                </th>

                <th>
                    Titre
                </th>

                <th>
                    Mots-clé
                </th>
            </tr>

        </table>
    </body>
    </html>
    """



    with open("public/index.html", "w", encoding='utf-8') as f:
        f.write(html)



def write(scene, title, url, keywords):
    if keywords:
        with open("public/index.html", "r+") as f:
            soup = BeautifulSoup(f.read(), "html.parser")

            table = soup.select("table")[0]
            newTr = soup.new_tag("tr")
            
            newTd = soup.new_tag('td')
            newTd.insert(0, NavigableString(str(scene)))
            newTr.append(newTd)

            newTd2 = soup.new_tag('td')

            a = soup.new_tag('a')
            fragment = "#:~:text="

            for keyword in  keywords:
                fragment += keyword+"&"
            
            a["href"] = url + fragment
            a["target"] = "_blank"
            a.insert(0, NavigableString(str(title)))
            newTd2.append(a)
            newTr.append(newTd2)

            newTd3 = soup.new_tag('td')
            newTd3.insert(0, NavigableString(str(keywords)))
            newTr.append(newTd3)

            table.append(newTr)
            
            f.seek(0)
            f.truncate()
            f.write(soup.prettify())
