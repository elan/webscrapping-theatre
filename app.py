import scrap
import result
import time


def main():
    start = time.time()

    result.delete()
    result.init()

    scrap.bonlieu("Bonlieu Annecy", "bonlieu")
    scrap.mc2("MC2 Grenoble", "mc2")
    scrap.lequaiAngers("Le Quai Angers", "lequai-angers")
    scrap.comedieCaen("Comédie Caen", "comedie-caen")
    scrap.tnpVilleurbanne("TNP Villeurbanne", "tnp-villeurbanne")
    scrap.lepreau("Le Préau Normandie", "lepreau")
    scrap.comedieReims("Comédie Reims", "comedie-reims")
    scrap.citeToulouse("Cité Toulouse", "cite-toulouse")
    scrap.cdnOrleans("CDN Orléans", "cdn-orleans")
    scrap.comedieValence("Comédie Valence", "comedie-valence")
    scrap.comedieColmar("Comédie Colmar", "comedie-colmar")
    scrap.lesAmandiers("Les Amandiers Nanterre", "les-amandiers")
    scrap.laManufacture("La Manufacture Nancy", "la-manufacture")
    scrap.theatrePublicMontreuil("Théâtre Public Montreuil", "theatre-public-montreuil")
    scrap.lesIlets("Les îlets Montluçon", "les-ilets")
    scrap.theatreNationalNice("Théâtre National Nice", "theatre-national-nice")
    scrap.comedieSaintEtienne("Comédie Saint-Etienne", "comedie-saint-etienne")
    scrap.treteauxFrance("Tréteaux de France", "treteaux-france")
    scrap.theatre13vents("Théâtre des 13 vents Montpellier", "theatre-13-vents")
    scrap.cdnFrontalierThionville("CDN Frontalier Thionville", "cdn-frontalier-thionville")
    scrap.tjpStrasbourg("TJP Strasbourg", "tjp-strasbourg")
    scrap.theatreSartrouvilleYvelines("Théâtre de Sartrouville et des Yvelines", "theatre-sartrouville-yvelines")
    scrap.leMetaPoitiers("Le Méta Poitiers", "le-meta-poitiers")
    scrap.tdbCdn("Théâtre Dijon Bourgogne", "tdb-cdn")
    scrap.t2g("Théâtre de Gennevilliers", "t2g")
    scrap.tqi("Théâtre des Quartiers d'Ivry", "tqi")
    scrap.lacommune("La Commune Aubervilliers", "lacommune")
    scrap.nouveauTheatre("Nouveau Théâtre Besançon", "nouveau-theatre")
    scrap.comedieBethune("Comédie Béthune", "comedie-bethune")
    scrap.theatreNationalBordeaux("Théâtre National Bordeaux", "theatre-national-bordeaux")
    scrap.cdnOceanIndien("CDN Océan Indien", "cdn-ocean-indien")
    scrap.cdnNomandieRouen("CDN Normandie-Rouen", "cdn-normandie-rouen")


    end = time.time()
    print("\n\nTemps total : " + str(round(end - start, 1)) + " secondes\n")

if __name__ == "__main__":
    main()
