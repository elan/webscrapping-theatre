# projet de webscrapping théâtre / IA

## A propos

### Dépendances

* python3
* beautifulsoup4 

```
apt install python3 python3-pip
pip install beautifulsoup4
```

### Utilisation
```
python3 app.py
```

### Notes
- la sortie se fait dans le fichier `public/index.html`
- un cache des fichiers HTML téléchargés est mis en place dans le dossier `cache/`. Chaque site scrappé dispose de son propre dossier.
- le script peut planter à cause de limitations des serveurs qu'on requêterait un peu trop vite. Ne pas hésiter à relancer le script, si le cache n'est pas vidé entre temps, cela reprendra les téléchargements des fichiers là où ça avait planté.

## Auteurs
voir [le fichier AUTHOR](AUTHOR)

## Licence
GNU AFFERO GENERAL PUBLIC LICENSE Version 3 
(voir [le fichier LICENSE](LICENSE))