import os
import requests
import codecs
import urllib3

def getPageContent(path, url, data=False):
    # Check whether the page is already cached
    path = "cache/" + path

    if (os.path.isfile(path)):
        with open(path, 'rb') as f:
            return f.read()

    urllib3.disable_warnings()

    # if not, execute a get or post request (based on the presence of parameters for now)
    print("## récupération de " + url)
    if (data == False):
        r = requests.get(url, verify=False)
    else:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36',
            'cache-control': 'private, max-age=0, no-cache'
        }
        r = requests.post(url, headers=headers, data=data)

    if r.status_code != 200:
        print("\t Erreur "+str(r.status_code))

    # cache the request result
    os.makedirs(os.path.dirname(path), exist_ok=True)
    with codecs.open(path, 'wb', errors='ignore') as file:
        file.write(r.content)

    # return content
    return r.content


# True if exists, False otherwise
def testPageExistence(url):
    exists = False
    response = requests.get(url)
    if response.status_code == 200: 
        exists = True    
    return(exists)