from bs4 import BeautifulSoup
import cache
import result
import re
import time
import json

def testKeywords(text):
    keywords = ["intelligence artificielle", "algorithmique", "algorithmiques", "informatique", "informatique quantique",  "complot", "complots", "hacker", "hackers","surveillance", "algorithme", "algorithmes", "logiciel", "logiciels", "ordinateur", "ordinateurs", "tiktok", "youtube", "twitter", "facebook", "instagram", "réseaux sociaux", "réseau social", "smartphone", "smartphones", "hacking", "hacker", "internet", "moteur de recherche","piratage", "profil", "web"]
    matchingKeywords = []
    for keyword in keywords:
        if re.search(rf"\b{keyword}\b", text, re.IGNORECASE):  
            matchingKeywords.append(keyword)

    return matchingKeywords
            
def begin(name):
    start = time.time()
    print ("############## " + name)
    print ("####################################")

    return start

def end(start):
    end = time.time()
    print("Exécuté en " + str(round(end - start, 1)) + " secondes\n")

    return

def comedieCaen(name, prefix):
    start = begin(name)
    
    seasons =  ["2015-2016", "2016-2017", "2017-2018", "2018-2019", "2019-2020", "2020-2021", "2021-2022", "2022-2023", "2023-2024"]
    for season in seasons:
        content = cache.getPageContent(prefix + "/" + season+".html", "https://www.comediedecaen.com/programmation/saisons/"+season+"/")
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select('div.pestak')
        for event in events:
            eventUrl = event.find('a')["href"]
            filename = eventUrl.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventUrl)
            eventSoup = BeautifulSoup(eventContent, "html.parser")

            titles = eventSoup.select("h1.titre")
            texts = eventSoup.select(".text-presentation")
            if(len(texts) > 0):
                title = titles[0].get_text()
                text = texts[0].get_text()
                matchingKeywords = testKeywords(text)
                result.write(name, title, eventUrl, matchingKeywords)

    end(start)
    
def cdnOrleans(name, prefix):
    start = begin(name)

    seasons =  ["1718", "1819", "1920", "2021", "2122", "2223"]
    for season in seasons:
        content = cache.getPageContent(prefix + "/" + season+".html", "http://www.cdn-orleans.com/voir-et-revoir/saison-"+season)
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select('.bloc-focus')
        for event in events:
            eventUrl = event["href"]
            filename = eventUrl.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventUrl)
            eventSoup = BeautifulSoup(eventContent, "html.parser")
            title = eventSoup.select("h1")
            if(len(title) < 1):
                title = eventSoup.select("h2")
            title = title[0].get_text()
            text = eventSoup.select(".cartouche")
            if(len(text) > 0):
                text = text[0].get_text()
                matchingKeywords = testKeywords(text)
                result.write(name, title, eventUrl, matchingKeywords)
    
    end(start)

def comedieColmar(name, prefix):
    start = begin(name)

    seasons =  ["archives/1", "archives/2", "archives/5", "saison"]
    for season in seasons:
        filename = season.replace("/", "-")
        content = cache.getPageContent(prefix + "/" + filename+".html", "https://comedie-colmar.com/"+season+"?cat=2")
        contentSoup = BeautifulSoup(content, "html.parser")
        events = contentSoup.select('.show-item')
        for event in events:
            eventUrl = event.find('a')["href"]
            filename = eventUrl.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventUrl)
            eventSoup = BeautifulSoup(eventContent, "html.parser")
            title = eventSoup.select(".show-page__title")[0].get_text()
            text = eventSoup.select(".text__content")[0].get_text()
            matchingKeywords = testKeywords(text)
            result.write(name, title, eventUrl, matchingKeywords)

    end(start)

def comedieValence(name, prefix):
    start = begin(name)

    seasons =  ["20-21", "21-22", "22-23", "23-24"]
    for season in seasons:
        content = cache.getPageContent(prefix + "/" + season +".html", "https://www.comediedevalence.com/saison_"+season)
        contentSoup = BeautifulSoup(content, "html.parser")
        events = contentSoup.select('.boite-evenement')
        for event in events:
            eventUrl = "https://www.comediedevalence.com"+event.find('a')["href"]
            filename = eventUrl.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventUrl)
            eventSoup = BeautifulSoup(eventContent, "html.parser")
            if eventSoup.select(".group-left"):
                title = eventSoup.select(".titre")[0].get_text()
                text = eventSoup.select(".group-left")[0].get_text()
                matchingKeywords = testKeywords(text)
                result.write(name, title, eventUrl, matchingKeywords)
    
    end(start)


def citeToulouse(name, prefix):
    start = begin(name)

    seasons =  ["2018-2019", "2019-2020", "2020-2021", "2021-2022", "2022-2023", "2023-2024"]

    for season in seasons:
        content = cache.getPageContent(prefix + "/" + season+".html", "https://theatre-cite.com/programmation/"+season+"/?filtre=theatre")
        contentSoup = BeautifulSoup(content, "html.parser")
        events = contentSoup.select('div.programmation-grid__item')
        for event in events:
            eventUrl = event.find('a')["href"]
            filename = eventUrl.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventUrl)
            eventSoup = BeautifulSoup(eventContent, "html.parser")
            title = eventSoup.select(".spectacle__title")[0].get_text()
            text = eventSoup.select(".spectacle__synopsis__content")[0].get_text()
            matchingKeywords = testKeywords(text)
            result.write(name, title, eventUrl, matchingKeywords)

    end(start)
            

def comedieReims(name, prefix):
    start = begin(name)

    seasons =  ["1920", "2021", "21-22", "22-23"]
    for season in seasons:
        content = cache.getPageContent(prefix + "/" + season + ".html", "https://www.lacomediedereims.fr/saison-"+season+"/")
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select('.boite-evenement')
        for event in events:
            eventUrl = "https://www.lacomediedereims.fr"+ event.find('a')["href"]
            filename = eventUrl.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventUrl)
            eventSoup = BeautifulSoup(eventContent, "html.parser")
            title = eventSoup.select(".titre")[0].get_text()
            texts = eventSoup.select(".courant")

            if(len(texts) > 0):
                text = texts[0].get_text()
            else:
                text = eventSoup.select(".chapeau")[0].get_text()
            
            matchingKeywords = testKeywords(text)
            result.write(name, title, eventUrl, matchingKeywords)

    end(start)


def tnpVilleurbanne(name, prefix):
    start = begin(name)

    firstSeason = 2007
    lastSeason = 2024
    done = []

    for i in range(firstSeason, lastSeason):
        seasonYear = str(i)
        content = cache.getPageContent(prefix + "/season" + seasonYear + ".html", "https://www.tnp-villeurbanne.com/programmation/?saison="+seasonYear)
        contentSoup = BeautifulSoup(content, "html.parser")
        events = contentSoup.select('.showBloc')
        for event in events:
            title = event.select(".titre-spectacle")[0].get_text()
            eventUrl = event.find('a')["href"]
            if eventUrl not in done:
                done.append(eventUrl)
                filename = eventUrl.replace("/", "-")
                eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventUrl)
                eventSoup = BeautifulSoup(eventContent, "html.parser")
                texts = eventSoup.select(".responsive-tabs div")
                if(len(texts) > 0):
                    text = texts[0].get_text()
                    matchingKeywords = testKeywords(text)
                    result.write(name, title, eventUrl, matchingKeywords)

    end(start)
                

def lequaiAngers(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://www.lequai-angers.eu/archives")
    contentSoup = BeautifulSoup(content, "html.parser")

    seasons = contentSoup.select('a.preview-subsection')
    for season in seasons:
        if "Saison" in season.get_text():
            url = "https://www.lequai-angers.eu" + season['href']
            filename = url.replace("/", "-")
            seasonContent = cache.getPageContent(prefix + "/"+filename+".html", url)
            seasonSoup = BeautifulSoup(seasonContent, "html.parser")

            linksGenre = seasonSoup.select('.genre-item')
            for linkGenre in linksGenre:
                if "théâtre" in linkGenre.get_text():
                    filteredUrl = "https://www.lequai-angers.eu" + linkGenre.select('a')[0]["href"]
                    filteredFilename = filteredUrl.replace("/", "-")

                    filteredSeasonContent = cache.getPageContent(prefix + "/"+filteredFilename+".html", filteredUrl)
                    filteredSeasonSoup = BeautifulSoup(filteredSeasonContent, "html.parser")

                    events = filteredSeasonSoup.select('li.card-item')
                    for event in events:
                        
                        eventUrl =  "https://www.lequai-angers.eu" + event.find("a")["href"]
                        eventFilename = eventUrl.replace("/", "-")
                        eventContent = cache.getPageContent(prefix + "/"+eventFilename+".html", eventUrl)
                        eventSoup = BeautifulSoup(eventContent, "html.parser")

                        title = eventSoup.select(".aside--title")[0].get_text()
                        summary = eventSoup.select(".main--summary")[0].get_text()
                        body = eventSoup.select(".main--body")[0].get_text()
                        text = summary + body

                        matchingKeywords = testKeywords(text)
                        result.write(name, title, eventUrl, matchingKeywords)

    end(start)


def mc2(name, prefix):
    start = begin(name)

    content = cache.getPageContent(prefix + "/main.html", "https://www.mc2grenoble.fr/programmation-passee/")
    contentSoup = BeautifulSoup(content, "html.parser")
    
    lastPageIndex = contentSoup.select('.page-numbers:not([class*="dots"]):not([class*="next"])')[-1].get_text()
    for i in range(1, int(lastPageIndex) + 1):
        pageName = str(i)
        subcontent = cache.getPageContent(prefix + "/archive"+pageName+".html", "https://www.mc2grenoble.fr/programmation-passee/page"+pageName)

        subcontentSoup = BeautifulSoup(subcontent, "html.parser")
        events = subcontentSoup.select('article.list-item')
        for event in events:
            title = event.find('h2', attrs={'class': "list-item-title"}).text
            tags = event.find('p', attrs={'class': "list-item-category"}).text
            if ("Théâtre" in tags):
                url = event.find('a')["href"]
                filename = url.replace("/", "-")
                eventContent = cache.getPageContent(prefix + "/" + filename+".html", url)
                text = BeautifulSoup(eventContent, "html.parser").select('.spectacle-content')[0].get_text()
                matchingKeywords = testKeywords(text)
                result.write(name, title, url, matchingKeywords)

    end(start)

def bonlieu(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://bonlieu-annecy.com/archives/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    years = contentSoup.find('select', attrs={'id': "archive"}).findAll("option")
    idsPage = [x["value"] for x in years]
    for id in idsPage:
        # pour chaque page on parcourt les "spectacles"
        pageContent = cache.getPageContent(prefix + "/archive"+id+".html", "https://bonlieu-annecy.com/wp-content/themes/Bonlieu/assets/php/filtre/getArchives.php", {'filtre': id})
        pageSoup = BeautifulSoup(pageContent, "html.parser")
        pageContents = pageSoup.findAll('div', attrs={'class': "lien-installation"})
        for content in pageContents:
            title = content.find('h2', attrs={'class': "installation-titres"}).text
            tags = content.find('h4', attrs={'class': "installation-cats"}).text
            # On ne garde que le théâtre
            if ("Théâtre" in tags):
                url = content.find('a')["href"]
                filename = url.replace("/", "-")
                eventContent = cache.getPageContent(prefix + "/" +filename+".html", url)
                eventContentSoup = BeautifulSoup(eventContent, "html.parser")
                text = eventContentSoup.select(".fiche-description")[0].get_text()
                matchingKeywords = testKeywords(text)
                result.write(name, title, url, matchingKeywords)   

    end(start)

def lepreau(name, prefix):
    start = begin(name)

    siteLink = "https://www.lepreaucdn.fr"

    seasons =  ["2018-2019", "2019-2020", "2020-2021", "2021-2022", "2022-2023","la-saison"]

    # récupération de la page principale des archives et de la saison actuelle    
    for season in seasons:
        if season == "la-saison":
            content = cache.getPageContent(prefix + "/main.html", siteLink + "/index.php/" + season + "/")
        else:
            content = cache.getPageContent(prefix + "/" + season + ".html", siteLink + "/index.php/archives/saison/" + season + "/")
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select('div.titre')
        for event in events:
            eventLink = siteLink + event.find('a')["href"]
            filename = eventLink.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventLink)
            eventSoup = BeautifulSoup(eventContent, "html.parser")

            title = eventSoup.select("h1")[0].getText()
            text = eventSoup.select(".presentation")[0].getText()
            if(len(text) > 0):
                matchingKeywords = testKeywords(text)
                result.write(name, title, eventLink, matchingKeywords)

    end(start)

def lesAmandiers(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://nanterre-amandiers.com/archives/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select(".item")
    # ajout de la saison actuelle
    seasonsLinks = ["https://nanterre-amandiers.com/saisons/saison-2023-2024/"]
    for season in seasons:
        seasonsLinks.append(season.find("a")["href"])

    for seasonLink in seasonsLinks:
        seasonYears = seasonLink.split("/")[-2]

        # on parcourt les spectacles
        seasonContent = cache.getPageContent(prefix + "/" + seasonYears+".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        events = seasonSoup.select("div.grid3 > a")
        for event in events:
            eventLink = event["href"]
            eventTitle = event["title"]
            filename = eventLink.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")

            if eventContentSoup.select(".textep"):
                text = eventContentSoup.select(".textep")[0].get_text()
                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)

    end(start)

def laManufacture(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://www.theatre-manufacture.fr/programmation/archives")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select(".list_editions > li > a")
    for season in seasons:
        seasonLink = season["href"]
        seasonYears = seasonLink.split("/")[-1]

        # on parcourt les spectacles
        seasonContent = cache.getPageContent(prefix + "/" +seasonYears+".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        events = seasonSoup.select(".tile__inner")
        for event in events:
            eventTitle = event.select("h3")[0].getText()
            eventLink = event.select("p.link > a")[0]["href"]
            filename = eventLink.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" +filename+".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")
            
            text = eventContentSoup.select(".presentation_text")[0].get_text()

            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)

    end(start)

def theatrePublicMontreuil(name, prefix):
    start = begin(name)

    siteLink = "https://theatrepublicmontreuil.com"

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", siteLink + "/archives")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select("a.main-grid-event-tile")
    # ajout de la saison actuelle
    seasonsLinks = ["/programme?subsections=saison_23-24"]
    for season in seasons:
        seasonsLinks.append(season["href"])

    for seasonLink in seasonsLinks:
        seasonYears = seasonLink.split("=")[-1].split("/")[-1]

        # on parcourt les spectacles
        seasonContent = cache.getPageContent(prefix + "/" +seasonYears + ".html", siteLink + seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        events = seasonSoup.select("a.main-grid-event-tile")
        for event in events:
            tags = event.find_all('div', attrs={'class': "h6"})
            for tag in tags:
                if ("Théâtre" in tag.text):
                    eventLink = siteLink + event["href"]
                    eventTitle = event.select("div.main-grid-event-tile__title > div.h2")[0].text
                    filename = eventLink.replace("/", "-")
                    eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventLink)
                    eventContentSoup = BeautifulSoup(eventContent, "html.parser")

                    textZone = eventContentSoup.select(".show-main-col")[1]
                    text = textZone.select(".show-main-col__tile")[0].text

                    matchingKeywords = testKeywords(text)
                    result.write(name, eventTitle, eventLink, matchingKeywords)

    end(start)

# Je ne suis pas certaine pour les mots-clefs pour trouver le théâtre,
# il y a beaucoup de mots dont je ne comprends pas trop la signification "spectacle-performance" par exemple ?
# j'ai fait le choix de récupérer toutes les pages taggées plus ou moins "spectacle" mais à vérifier
def lesIlets(name, prefix):
    start = begin(name)

    siteLink = "https://www.theatredesilets.fr"

    seasons =  ["2018-2019", "2019-2020", "2020-2021", "2021-2022", "2022-2023","2023-2024"]

    # on parcourt les saisons   
    for season in seasons:
        content = cache.getPageContent(prefix + "/season_" + season + ".html", siteLink + "/saison/saison-" + season + "/")
        contentSoup = BeautifulSoup(content, "html.parser")

        # on parcourt les événements
        events = contentSoup.select("main.wrapper article.wrap")
        for event in events:
            eventTitle = event.select("header > h1")[0].text
            eventLink = event.select("ul.go--right > li > a")[0]["href"]
            
            # on choisit les spectacles
            tags = event.select("ul.go--bottom")[0].find_all('li', attrs={'itemprop': "additionalType"})
            for tag in tags:
                if ("spectacle" in tag.text):
                    filename = eventLink.replace("/", "-")
                    eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventLink)
                    eventSoup = BeautifulSoup(eventContent, "html.parser")

                    text = eventSoup.find("div", attrs={"class": "content", "itemprop": "description"}).text
                    matchingKeywords = testKeywords(text)
                    result.write(name, eventTitle, eventLink, matchingKeywords)        

    end(start)

##########################################
# REMARQUE : je ne trouve pas les archives
##########################################
def theatreNationalNice(name, prefix):
    start = begin(name)

    # récupération de la page des spectacles passés
    pastContent = cache.getPageContent(prefix + "/season_2023-2024_past.html", "https://www.tnn.fr/fr/spectacles/saison-2023-2024/spectacles-passes")
    pastContentSoup = BeautifulSoup(pastContent, "html.parser")
    # récupération de la page des spectacles à venir
    content = cache.getPageContent(prefix + "/season_2023-2024_to-come.html", "https://www.tnn.fr/fr/spectacles/saison-2023-2024")
    contentSoup = BeautifulSoup(content, "html.parser")

    # on parcourt les spectacles
    events = contentSoup.select("a.item")
    events += pastContentSoup.select("a.item")
    for event in events:
        eventLink = event["href"]
        eventTitle = event.select("figcaption > h3")[0].text
        tags = event.select("figcaption > h4")[0].text
        if ("Théâtre" in tags):
            filename = eventLink.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventLink)
            eventSoup = BeautifulSoup(eventContent, "html.parser")

            text = eventSoup.select("div.description")[0].text
            
            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)  

    end(start)

def comedieSaintEtienne(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://www.lacomedie.fr/la-saison/archives/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select("div.subpage > a")
    # ajout de la saison actuelle
    seasonsLinks = ["https://www.lacomedie.fr/la-saison/programmation-la-comedie/"]
    # récupération de toutes les saisons précédentes
    for season in seasons:
        seasonLink = season["href"]
        ########################
        # Dans archives-2011-a-2021 on a des pdf à chaque année, pour linstant on met ça de côté
        ########################
        if "archives-2011-a-2021" not in seasonLink:
            seasonsLinks.append(seasonLink)
    
    # on parcourt les saisons
    for seasonLink in seasonsLinks:
        seasonYears = seasonLink.split("/")[-2]

        # on parcourt les spectacles
        seasonContent = cache.getPageContent(prefix + "/" +seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        events = seasonSoup.select("article > header > h3.titre > a")
        for event in events:
            eventLink = event["href"]
            eventTitle = event.text
            filename = eventLink.replace("/", "-")
            eventContent = cache.getPageContent(prefix + "/" + filename+".html", eventLink)
            eventSoup = BeautifulSoup(eventContent, "html.parser")

            textZone = eventSoup.select("#main > article")
            # beaucoup de pages de description de spectacle renvoient une erreur 404
            if textZone != []:
                text = textZone[0].find("div", attrs={"itemprop": "articleBody"}).text
            
                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)  

    end(start)

def theatre13vents(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://www.13vents.fr/le-theatre/les-archives/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select("article > h2 > a")
    for season in seasons:
        seasonLink = season["href"]
        seasonYears = seasonLink.split("/")[-2]

        seasonContent = cache.getPageContent(prefix + "/" + seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        # on parcourt les spectacles
        events = seasonSoup.select("h2.extrait-titre > a")
        for event in events:
            eventTitle = event.text
            eventLink = event["href"]
            filename = eventLink.replace("/", "-")

            eventContent = cache.getPageContent(prefix + "/" + filename +".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")

            text = eventContentSoup.select(".article-txt")[0].get_text()

            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

def treteauxFrance(name, prefix):
    start = begin(name)

    siteLink = "https://www.treteauxdefrance.com/repertoire"

    urlToComplete = siteLink + "?page="
    pageNum = 1
    eventLim = 9
    newPage = True

    while newPage:
        eventNum = 0

        # récupération des pages des spectacles
        content = cache.getPageContent(prefix + "/main" + str(pageNum) + ".html", urlToComplete + str(pageNum))
        contentSoup = BeautifulSoup(content, "html.parser")

        # on parcourt les spectacles
        events = contentSoup.select("a.preview")
        for event in events:
            eventNum += 1

            eventLink = event["href"]
            filename = eventLink.replace("/", "-")
            eventTitle = event.select("div.infos > h2.title")[0].text

            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", siteLink + eventLink)
            eventSoup = BeautifulSoup(eventContent, "html.parser")

            text = eventSoup.select(".drops__right")[0].text
            
            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)  
        
        # On cherche la page suivante des spectacles
        if eventNum == eventLim:
            pageNum += 1
            newPage = cache.testPageExistence(urlToComplete + str(pageNum))
        else:
            newPage = False

    end(start)

def cdnFrontalierThionville(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://www.nest-theatre.fr/toutes-les-saisons")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select(".archive")
    for season in seasons:
        links = season.select("a")[-1]
        seasonLink = links["href"]
        seasonYears = seasonLink.split("/")[-1]

        seasonContent = cache.getPageContent(prefix + "/" +seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        # on parcourt les spectacles
        events = seasonSoup.select(".item__content")
        for event in events:
            eventTitle = event.select("header > h1")[0].text
            eventLink = event.select("a")[0]["href"]
            filename = eventLink.replace("/", "-")

            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")

            if eventContentSoup.select(".module > .texte"):
                text = eventContentSoup.select(".module > .texte")[0].get_text()

                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

# Données dans le script js
# Je n'ai pas trouvé les archives
def tjpStrasbourg(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://tjp-strasbourg.com/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # récupération de la variable js contenant les données
    scripts = contentSoup.findAll("script", attrs={"type": "text/javascript"})
    for script in scripts:
        dataTable = re.search('var eventsData = ({.*});', script.text, flags=re.M|re.S)
        if dataTable:
            dataTable = dataTable.group(1)

            # récupération du tableau de données sur les événements
            events = re.search('events: (\[{.*}\]),', script.text, flags=re.M)
            events = json.loads(events.group(1))

            # réupération des infos des spectacles
            for event in events:
                if(event["event_type"] == 30): # type 30 = spectacle
                    eventTitle = event["title"]
                    eventLink = event["url"]
                    filename = eventLink.replace("/", "-")

                    eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
                    eventContentSoup = BeautifulSoup(eventContent, "html.parser")

                    if eventContentSoup.select(".detail-text"):
                        text = eventContentSoup.select(".detail-text")[0].get_text()

                        matchingKeywords = testKeywords(text)
                        result.write(name, eventTitle, eventLink, matchingKeywords)
    
    end(start)

# Je n'ai pas sélectionné les genres car je ne savais pas lesquels prendre (ex. danse-théâtre ?)
# Certains évènements en double, pourquoi ? (url identique mais pas de doublon dans le tableau events => double référencement sur le site ?)
def theatreSartrouvilleYvelines(name, prefix):
    start = begin(name)

    siteLink = "http://www.theatre-sartrouville.com"

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", siteLink + "/archives/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select(".cat-item > a")
    # ajout de la saison actuelle (2024-2025)
    seasonsLinks = [siteLink + "/les-spectacles/"]

    # récupération de toutes les saisons précédentes
    for season in seasons:
        seasonsLinks.append(siteLink + season["href"])
    
    # on parcourt les saisons
    for seasonLink in seasonsLinks:
        seasonYears = seasonLink.split("/")[-2]

        seasonContent = cache.getPageContent(prefix + "/" + seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        events = []

        if seasonYears == "les-spectacles":
            events = seasonSoup.select(".item-event > a")
        # Dans les archives les événements sont parfois sur plusieurs pages différentes
        else:
            urlToComplete = seasonLink + "page/"
            pageNum = 1
            newPage = True
            while newPage:
                # récupération des pages des spectacles
                content = cache.getPageContent(prefix + "/" + seasonYears + "_p" + str(pageNum) + ".html", urlToComplete + str(pageNum))
                contentSoup = BeautifulSoup(content, "html.parser")
                eventsFromNewPage = contentSoup.select(".item-event > a")
                # on vérifie si la page est vide
                if eventsFromNewPage == []:
                    newPage = False
                # et on ajoute les spectacle
                else:
                    for eventFromNewPage in eventsFromNewPage:
                        events.append(eventFromNewPage)                
                    # On cherche la page suivante des spectacles
                    pageNum += 1

        # on parcourt les spectacles
        for event in events:
            eventTitle = event.select(".item-event-title")[0].text
            eventLink = event["href"]
            filename = eventLink.replace("/", "-")

            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")

            if eventContentSoup.select(".entry-titles"):
                text = eventContentSoup.select(".entry-titles")[0].get_text()

                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

def leMetaPoitiers(name, prefix):
    start = begin(name)

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", "https://le-meta.fr/")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select("#menu-archive > li > a")
    # ajout de la saison actuelle (2025)
    seasonsLinks = ["https://le-meta.fr/saison/"]

    # récupération de toutes les saisons précédentes
    for season in seasons:
        seasonsLinks.append(season["href"])
    
    # on parcourt les saisons
    for seasonLink in seasonsLinks:
        seasonYears = seasonLink.split("/")[-2]

        seasonContent = cache.getPageContent(prefix + "/" +seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        # on parcourt les spectacles
        events = seasonSoup.select("figure > a")
        for event in events:
            eventTitle = event.text
            eventLink = event["href"]
            filename = eventLink.replace("/", "-")

            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")

            if eventContentSoup.select(".content"):
                text = eventContentSoup.select(".content")[0].get_text()

                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

def tdbCdn(name, prefix):
    start = begin(name)

    siteLink = "https://www.tdb-cdn.com/fr"

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", siteLink + "/archives")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select("a.list-subsections__subsection__title")
    # ajout de la saison actuelle (2025)
    seasonsLinks = [siteLink + "/toute-la-saison"]

    # récupération de toutes les saisons précédentes
    for season in seasons:
        seasonsLinks.append(siteLink + season["href"])
    
    # on parcourt les saisons
    for seasonLink in seasonsLinks:
        seasonYears = seasonLink.split("/")[-2]

        seasonContent = cache.getPageContent(prefix + "/" +seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        # on parcourt les spectacles
        events = seasonSoup.select(".preview.o-js-preview > a")
        for event in events:
            if event.select(".preview__infos__left__tag")[0].text == "SPECTACLES":
                eventTitle = event.select(".preview__infos__bottom__title")[0].text
                eventLink = siteLink + event["href"]
                filename = eventLink.replace("/", "-")

                eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
                eventContentSoup = BeautifulSoup(eventContent, "html.parser")

                if eventContentSoup.select(".main-content__body__left-body"):
                    text = eventContentSoup.select(".main-content__body__left-body")[0].get_text()

                    matchingKeywords = testKeywords(text)
                    result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

# Archives non traitées car en pdf
def t2g(name, prefix):
    start = begin(name)

    # récupération de la page principale (saison actuelle - 2025)
    content = cache.getPageContent(prefix + "/main.html", "https://theatredegennevilliers.fr/la-saison/programmation")
    contentSoup = BeautifulSoup(content, "html.parser")

    # on parcourt les spectacles
    events = contentSoup.select("article > a")
    for event in events:
        eventTitle = event.text
        eventLink = event["href"]
        filename = eventLink.replace("/", "-")

        eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
        eventContentSoup = BeautifulSoup(eventContent, "html.parser")
        
        if eventContentSoup.select("article.text"):
            text = eventContentSoup.select("article.text")[0].get_text()

            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

def tqi(name, prefix):
    start = begin(name)

    siteLink = "https://www.theatre-quartiers-ivry.com"
    showLink = siteLink + "/saison/spectacle/"

    # récupération de la page principale des archives
    content = cache.getPageContent(prefix + "/main.html", siteLink + "/saison/saisons-precedentes.htm")
    contentSoup = BeautifulSoup(content, "html.parser")

    # chaque année d'archive est sur une page dédiée
    seasons = contentSoup.select(".listeSaison > li > a")
    # ajout de la saison actuelle (2025)
    seasonsLinks = [siteLink + "/saison/"]

    # récupération de toutes les saisons précédentes
    for season in seasons:
        seasonsLinks.append(siteLink + season["href"])
    
    # on parcourt les saisons
    for seasonLink in seasonsLinks:
        archive = True
        seasonYears = seasonLink.split("/")[-1].split(".")[0]
        if seasonYears == "":
            archive = False
            seasonYears = "saison-actuelle"

        seasonContent = cache.getPageContent(prefix + "/" +seasonYears + ".html", seasonLink)
        seasonSoup = BeautifulSoup(seasonContent, "html.parser")

        # on parcourt les spectacles
        if not(archive):
            eventsPerSeasons = ["https://www.theatre-quartiers-ivry.com/json/JSON_spectacle.json", "https://www.theatre-quartiers-ivry.com/json/JSON_spectacle.json?command=getSpectaclesPasses"]
        else:
            eventsPerSeasons = ["https://www.theatre-quartiers-ivry.com/json/JSON_spectacle.json?archives=1&fileName=" + seasonYears]

        for eventsPerSeason in eventsPerSeasons:
            jsonName = eventsPerSeason.replace("/", "-")
            eventsInfos = cache.getPageContent(prefix + "/" + jsonName + ".html", eventsPerSeason)
            events = json.loads(eventsInfos)

            for event in events["spectacles"]:
                if event["mc6"] == "Théâtre":
                    eventTitle = event["ls"]
                    eventLink = showLink + event["fn"] + ".htm"
                    if archive:
                        eventLink += "?archives=1"
                    filename = eventLink.replace("/", "-")

                    eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
                    eventContentSoup = BeautifulSoup(eventContent, "html.parser")

                    if eventContentSoup.select("#sectionGauche"):
                        text = eventContentSoup.select("#sectionGauche")[0].get_text()

                        matchingKeywords = testKeywords(text)
                        result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

# Archives non traitées car en pdf
def lacommune(name, prefix):
    start = begin(name)

    # récupération de la page principale (saison actuelle 2024 - 2025)
    content = cache.getPageContent(prefix + "/main.html", "https://www.lacommune-aubervilliers.fr/programmation/#01_theatre-saison-24-25")
    contentSoup = BeautifulSoup(content, "html.parser")

    # on parcourt les spectacles
    events = contentSoup.select(".item-box > .info")
    for event in events:
        eventTitle = event.select(".item-box-titre")[0].get_text()
        eventLink = event.select(".item-link")[0]["href"]
        filename = eventLink.replace("/", "-")

        eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
        eventContentSoup = BeautifulSoup(eventContent, "html.parser")
        
        if eventContentSoup.select(".wpb_content_element.description-2020 > .wpb_wrapper"):
            text = eventContentSoup.select(".wpb_content_element.description-2020 > .wpb_wrapper")[0].get_text()
            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

# Archives non traitées car en pdf
def nouveauTheatre(name, prefix):
    start = begin(name)

    # récupération de la page principale (saison actuelle 2024 - 2025)
    content = cache.getPageContent(prefix + "/main.html", "https://ntbesancon.fr/programme")
    contentSoup = BeautifulSoup(content, "html.parser")

    # on parcourt les spectacles
    events = contentSoup.select(".event-card.grid__item")
    for event in events:
        # Je ne sais pas pourquoi ici .text et .getText() ne fonctionnent pas, donc j'ai utilisé .string
        eventTitle = event.select(".event-card__infos > div > h5")[0].string
        eventLink = event.select(".event-card__link")[0]["href"]
        filename = eventLink.replace("/", "-")

        eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
        eventContentSoup = BeautifulSoup(eventContent, "html.parser")
        
        if eventContentSoup.select(".presentation__text"):
            text = eventContentSoup.select(".presentation__text")[0].get_text()
            
            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

def comedieBethune(name, prefix):
    start = begin(name)
    
    seasons =  ["2014-2015", "2015-2016", "2016-2017", "2017-2018", "2018-2019", "2019-2020", "2020-2021", "2021-2022", "2022-2023", "2023-2024", "2024-2025"]
    for season in seasons:
        content = cache.getPageContent(prefix + "/" + season+".html", "https://www.comediedebethune.org/saison/"+season+"/")
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select('.card-info > .stretched-link')
        for event in events:
            eventTitle = event.select(".card-title")[0].text
            eventLink = event["href"]
            filename = eventLink.replace("/", "-")

            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")
            
            if eventContentSoup.select("#description .col-12"):
                text = eventContentSoup.select("#description .col-12")[0].get_text()
                
                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)

    end(start)

def theatreNationalBordeaux(name, prefix):
    start = begin(name)

    archivedSiteLink = "https://archives.tnba.org"
    siteLink = "https://www.tnba.org"
    
    archivedSeasons =  ["2014-2015", "2015-2016", "2016-2017", "2017-2018", "2018-2019"]    
    seasons =  ["2019-2020", "2020-2021", "2021-2022", "2022-2023", "2023-2024", "2024-2025"]

    for season in archivedSeasons:
        
        content = cache.getPageContent(prefix + "/main-archives.html", archivedSiteLink + "/page/saison-" + season)
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select(".views-row")
        for event in events:
            if event.select(".views-field-field-typologie-evt > div")[0].text == "Théâtre":
                eventInfos = event.select(".views-field-title")[0]
                eventTitle = eventInfos.text
                eventLink = eventInfos.select("a")[0]["href"]
                filename = eventLink[1:len(eventLink)].replace("/", "-")

                eventContent = cache.getPageContent(prefix + "/" + filename + ".html", archivedSiteLink + eventLink)
                eventContentSoup = BeautifulSoup(eventContent, "html.parser")
                
                if eventContentSoup.select(".field-type-text-with-summary"):
                    text = eventContentSoup.select(".field-type-text-with-summary")[0].get_text()
                    
                    matchingKeywords = testKeywords(text)
                    result.write(name, eventTitle, eventLink, matchingKeywords)

    for season in seasons:
        
        content = cache.getPageContent(prefix + "/main.html", siteLink + "/saison/" + season)
        contentSoup = BeautifulSoup(content, "html.parser")

        events = contentSoup.select(".node__header")
        for event in events:
            eventTitle = event.select(".node__title")[0].text
            eventLink = event.select(".node__title > a")[0]["href"]
            filename = eventLink[1:len(eventLink)].replace("/", "-")

            eventContent = cache.getPageContent(prefix + "/" + filename + ".html", siteLink + eventLink)
            eventContentSoup = BeautifulSoup(eventContent, "html.parser")
            
            if eventContentSoup.select(".node__content"):
                text = eventContentSoup.select(".node__content")[0].get_text()
                
                matchingKeywords = testKeywords(text)
                result.write(name, eventTitle, eventLink, matchingKeywords)

    end(start)

# Pas d'archives
def cdnOceanIndien(name, prefix):
    start = begin(name)

    siteLink = "https://www.cdnoi.re"

    # récupération de la page principale (saison actuelle 2024 - 2025)
    content = cache.getPageContent(prefix + "/main.html", siteLink + "/programmation/&all=1")
    contentSoup = BeautifulSoup(content, "html.parser")

    # on parcourt les spectacles
    events = contentSoup.select(".prog_content > h3 > a")
    for event in events:
        eventTitle = event.text
        eventLink = event["href"]
        filename = eventLink.replace("/", "-")

        eventContent = cache.getPageContent(prefix + "/" + filename + ".html", siteLink + eventLink)
        eventContentSoup = BeautifulSoup(eventContent, "html.parser")
        
        if eventContentSoup.select(".content"):
            text = eventContentSoup.select(".content")[0].get_text()
            
            matchingKeywords = testKeywords(text)
            result.write(name, eventTitle, eventLink, matchingKeywords)
      
    end(start)

def cdnNomandieRouen(name, prefix):
    start = begin(name)

    siteLink = "https://www.cdn-normandierouen.fr/saison/"
    
    # saison acutelle (2024-2025) référencée différement
    seasons =  ["2020-2021", "2021-2022", "2022-2023", "2023-2024", "spectacles/#agenda"]
    
    # Liste des spectacles car certains sont rejoués sur plusieurs années
    allEvents = []

    for season in seasons:
        
        content = cache.getPageContent(prefix + "/main.html", siteLink + season)
        contentSoup = BeautifulSoup(content, "html.parser")

        # on parcourt les spectacles
        events = contentSoup.select(".entry-content")
        for event in events:
            eventTitle = event.select(".titrespectaclecalendrier")[0].text
            if eventTitle not in allEvents:
                allEvents.append(eventTitle)
                eventLink = event.select("a")[0]["href"]
                filename = eventLink.replace("/", "-")

                # print(eventTitle)

                eventContent = cache.getPageContent(prefix + "/" + filename + ".html", eventLink)
                eventContentSoup = BeautifulSoup(eventContent, "html.parser")

                if eventContentSoup.select("span[itemprop='description']"):
                    text = eventContentSoup.select("span[itemprop='description']")[0].get_text()
                    
                    matchingKeywords = testKeywords(text)
                    result.write(name, eventTitle, eventLink, matchingKeywords)
    
    end(start)